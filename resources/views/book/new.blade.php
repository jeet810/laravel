@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">New Book</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/book/save') }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">Book Title</label>

                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control" name="title" required>

                                
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-md-4 control-label">Book ID</label>

                            <div class="col-md-6">
                                <input id="password" type="text" class="form-control" name="bookid" required>

                                
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-md-4 control-label">Book Status</label>

                            <div class="col-md-6">
                                <select class="form-control" name="status">
                                	<option value="1">LOCK</option>
                                	<option value="0">UNLOCK</option>
                                </select>
                                
                            </div>
                        </div>

                        
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-sign-in"></i> Save
                                </button>

                                <a class="btn btn-link" href="{{ url('/book/all') }}">View All</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
