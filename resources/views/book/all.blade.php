@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">All Books</div>
                <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                    	<tr>
                    		<td>Sr.</td>
                    		<td>ID</td>
                    		<td>Title</td>
                    		<td>Status</td>
                    		<td>Action</td>
                    	</tr>
                    	
                    	@foreach($books as $b)
                    	<tr>
                    		<td>{{$b->id}}</td>
                    		<td>{{$b->id}}</td>
                    		<td>{{$b->title}}</td>
                    		@if($b->status)
                    		<td>LOCKED</td>
                    		@else
                    		<td>UNLOCKED</td>
                    		@endif
                    		
                    			@if($b->status)
                    		<td><a href="{{url('book/unlock',$b->id)}}" class="btn btn-success">Unlock Now</a></td>
                    		@else
                    		<td><a href="{{url('book/lock',$b->id)}}" class="btn btn-danger">Lock Now</a></td>
                    		@endif
                    		
                    	</tr>
                    	@endforeach
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
