<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/welcome','WelcomeController@index');
Route::get('/welcomeagain','WelcomeController@doublewelcome');
Route::get('/Login',function(){
	return View::make('Login');	
	
});
Route::get('/book/{status}/{id}','BookController@update');
Route::get('/book/all','BookController@all');
Route::get('/book/new','BookController@newbook');
Route::post('/book/save','BookController@store');
Route::get('/book/all','BookController@all');
Route::auth();

Route::get('/home', 'HomeController@index');
