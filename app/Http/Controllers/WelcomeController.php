<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class WelcomeController extends Controller
{
    //
    public function index(){

    	// return "Hello I am a method in Welcome controller";
    	return view('login');
    }

    public function doublewelcome(){
    	return "I again welcome you...";
    }
}
