<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Book;

class BookController extends Controller
{
    //
    public function newbook(){
    	return view('book.new');
    }

    public function store(Request $request){
    	$title  = $request->title;
    	$bookid  = $request->bookid;
    	$status  = $request->status;

    	$b = Book::create();
    	$b->title = $title;
    	$b->bookid = $bookid;
    	$b->status = $status;
    	if($b->save())
    		return redirect()->back();
    	else
    		return "Try again";

    	// return;
    }

    public function all(){
    	$b = Book::all();

    	return view('book.all')->with('books',$b);
    }

    public function update($status,$id){
    	
    	$b = Book::find($id);
    	if($status=='unlock'){
    		$b->status = false;
    	}
    	else{
    		$b->status = true;
    	}


    	if($b->save())
    		return redirect()->back();
    	else
    		return "Try again";

    	// return;
    }
}
